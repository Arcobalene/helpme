export function ComponentsConfig () {
  return {
    tabs: new Tabs(),
    items: new Items(),
    SolutionItems: new SolutionItems()
  }
}

export function Tabs () {
  return [
    { id: 'laptop', title: 'Ноутбук' },
    { id: 'printer', title: 'Принтер' },
    { id: 'wifi', title: 'Wi-FI' }
  ]
}

export function Items () {
  return [
    {
      id: 'laptop',
      list: [
        {
          title: 'Не включается ноутбук',
          subtitle: 'Описание проблемы',
          image: 'https://cdn.shopify.com/s/files/1/0101/4864/2879/products/5571-a_large.jpg?v=1665638170',
          solutionId: '1'
        }
      ]
    },
    {
      id: 'printer',
      list: [
        {
          title: 'Не включается принтер',
          subtitle: 'Описание проблемы',
          image: 'https://ultratrade.ru/images/gallery/5/1-193889_1658153406.jpg',
          solutionId: '2'
        }
      ]
    },
    {
      id: 'wifi',
      list: [
        {
          title: 'Не включается Wi-Fi',
          subtitle: 'Описание проблемы',
          image: 'https://www.lifewire.com/thmb/izQkLcZUiSnIJyuAfLlOW22AqcM=/900x0/filters:no_upscale():max_bytes(150000):strip_icc()/Asus-ROG-Rapture-GT-AX11000-165680dc111f4bfab7e14707978dab4a.jpg',
          solutionId: '3'
        }
      ]
    }
  ]
}

export function SolutionItems () {
  return [
    {
      id: '1',
      tabs: [
        { id: 'step-1', title: 'Шаг 1' },
        { id: 'step-2', title: 'Шаг 2' },
        { id: 'step-3', title: 'Шаг 3' },
        { id: 'step-4', title: 'Шаг 4' },
        { id: 'step-5', title: 'Шаг 5' }
      ],
      items: [
        {
          id: 'step-1',
          title: 'Не включается ноутбук',
          subtitle: 'Описание проблемы',
          list: ['Список 1', 'Список 2', 'Список 3'],
          image: 'https://cdn.shopify.com/s/files/1/0101/4864/2879/products/5571-a_large.jpg?v=1665638170'
        }
      ]
    }
  ]
}
