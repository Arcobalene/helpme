export default {
  ssr: false,
  target: 'server',
  head: {
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },
  loading: { color: '#fff' },
  css: [
    { src: 'bootstrap/dist/css/bootstrap.min.css', lang: 'css' },
    { src: '@fortawesome/fontawesome-free/css/all.min.css', lang: 'css' },
    { src: 'animate.css/animate.min.css', lang: 'css' },
    { src: '~assets/app/app.css', lang: 'css' },
    { src: 'animate.css/animate.min.css', lang: 'css' },
    { src: 'owl.carousel/dist/assets/owl.carousel.css', lang: 'css' },
    { src: 'owl.carousel/dist/assets/owl.theme.default.css', lang: 'css' },
    { src: '~/assets/css/Chart.min.css', lang: 'css' }
  ],
  plugins: [
    '~plugins/theme.js'
  ],
  router: {
    base: '/',
    linkActiveClass: 'current-menu-item',
    linkExactActiveClass: 'current-menu-item'
  },
  buildModules: [],
  modules: [
    'bootstrap-vue/nuxt',
    '@nuxtjs/axios',
    '@nuxtjs/proxy',
    '@nuxtjs/pwa'
  ],
  axios: {
    baseURL: '/',
    browserBaseURL: '/',
    proxy: false
  },
  proxy: {
    '/api': {
      target: 'http://176.96.243.111:8083/',
      secure: false,
      pathRewrite: { '^/api': '/api/' }
    }
  },
  build: {
    extractCSS: {
      allChunks: true
    },
    vendor: ['jquery', 'bootstrap'],
    extend (config, ctx) {
    },
    babel: {
      compact: true,
      plugins: [
        ['@babel/plugin-proposal-private-methods', { loose: true }],
        ['@babel/plugin-proposal-private-property-in-object', { loose: true }]
      ]
    }
  },
  server: {
    port: 3000,
    host: '0.0.0.0'
  },
  telemetry: false
}
